class AvailableDaysController < ApplicationController
  before_action :set_available_day, only: [:show, :update, :destroy]

  # GET /available_days
  def index
    @available_days = AvailableDay.all

    render json: @available_days
  end

  # GET /available_days/1
  def show
    render json: @available_day
  end

  # POST /available_days
  def create
    if !AvailableDay.exists?(date: params[:date], User_id: params[:User_id])
      @available_day = AvailableDay.new(available_day_params)

      if @available_day.save
        render json: @available_day, status: :created, location: @available_day
      end
    end
  end

  # PATCH/PUT /available_days/1
  def update
    if @available_day.update(available_day_params)
      render json: @available_day
    else
      render json: @available_day.errors, status: :unprocessable_entity
    end
  end

  # DELETE /available_days/1
  def destroy
    @available_day.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_available_day
      @available_day = AvailableDay.find(params[:id])
    end

    # Only allow a trusted parameter "white list" through.
    def available_day_params
      params.require(:available_day).permit(:date, :User_id)
    end
end
