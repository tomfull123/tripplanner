class AvailableDay < ApplicationRecord
  belongs_to :User
  validates :date, uniqueness: { scope: :User_id }
end
