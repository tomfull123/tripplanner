class CreateAvailableDays < ActiveRecord::Migration[5.2]
  def change
    create_table :available_days do |t|
      t.date :date
      t.integer :userId
      t.references :User, foreign_key: true

      t.timestamps
    end
  end
end
