class AddIndexToavailableDays < ActiveRecord::Migration[5.2]
  def change
    add_index :available_days, [:date, :User_id], unique: true
  end
end
