class RemoveUserIdFromAvailableDays < ActiveRecord::Migration[5.2]
  def change
    remove_column :available_days, :userId, :integer
  end
end
