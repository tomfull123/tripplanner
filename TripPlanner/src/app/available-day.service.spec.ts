import { TestBed, inject } from '@angular/core/testing';

import { AvailableDayService } from './available-day.service';

describe('AvailableDayService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [AvailableDayService]
    });
  });

  it('should be created', inject([AvailableDayService], (service: AvailableDayService) => {
    expect(service).toBeTruthy();
  }));
});
