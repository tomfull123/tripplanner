import { Component, OnInit, ChangeDetectionStrategy, ViewChild, TemplateRef, ViewEncapsulation } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { CalendarEvent, CalendarEventAction, CalendarEventTimesChangedEvent, CalendarMonthViewComponent, CalendarMonthViewDay } from 'angular-calendar';
import { AvailableDayService } from '../available-day.service';
import { AvailableDay } from '../available-day';
import { UserService } from '../user.service';
import { User } from '../user';

const colors: any = {
  red: {
    primary: '#ad2121',
    secondary: '#FAE3E3'
  },
  blue: {
    primary: '#1e90ff',
    secondary: '#D1E8FF'
  },
  yellow: {
    primary: '#e3bc08',
    secondary: '#FDF1BA'
  },
  green: {
    primary: '#228B22',
    secondary: '#98fb98'
  }
};

@Component({
  selector: 'app-calendar',
  changeDetection: ChangeDetectionStrategy.OnPush,
  encapsulation: ViewEncapsulation.None,
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
export class CalendarComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  calendar: CalendarMonthViewComponent;

  view: string = 'month';

  viewDate: Date = new Date("09/01/2018");

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-fw fa-pencil"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.handleEvent('Edited', event);
      }
    },
    {
      label: '<i class="fa fa-fw fa-times"></i>',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        this.events = this.events.filter(iEvent => iEvent !== event);
        this.handleEvent('Deleted', event);
      }
    }
  ];

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  days: AvailableDay[];

  calendarDays: CalendarMonthViewDay[];

  users: User[];
  userCount: number;

  constructor(
    private modal: NgbModal,
    private availableDayService: AvailableDayService,
    private userService: UserService
  ) {}

  ngOnInit() {
    this.getAvailableDays();
  }

  getAvailableDays(): void {
    this.availableDayService.getAvailableDays()
      .subscribe(days => { 
        this.days = days;
        this.getUsers();
      });
  }

  getUsers(): void {
    this.userService.getUsers()
      .subscribe(users => { 
        this.users = users;
        this.userCount = users.length;
        this.addAllEvents();
      });
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    
    if (isSameMonth(date, this.viewDate)) {
      this.viewDate = date;
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  beforeMonthViewRender({ body }: { body: CalendarMonthViewDay[] }): void {

    if (!this.users || this.users.length == 0) return;

    body.forEach(day => {
      if (day.badgeTotal >= this.users.length) {
        day.cssClass = 'selected-cal-day';
      }
    });
  }

  refreshView(): void {
    this.refresh.next();
  }
  
  private addAllEvents(): void {

    for (let day of this.days) {
      var user: User = this.users.find(user => user.id === day.User_id);

      this.events.push({
        title: user.name,
        start: startOfDay(new Date(day.date)),
        end: endOfDay(new Date(day.date)),
        color: colors.red,
        draggable: true,
        resizable: {
          beforeStart: true,
          afterEnd: true
        }
      });

    }

    this.refreshView();
  }

}

