import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { AvailableDayService } from '../available-day.service';
import { ChangeDetectionStrategy, ViewChild, TemplateRef } from '@angular/core';
import { startOfDay, endOfDay, subDays, addDays, endOfMonth, isSameDay, isSameMonth, addHours } from 'date-fns';
import { Subject } from 'rxjs';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap/modal/modal.module';
import { CalendarEvent, CalendarMonthViewDay, CalendarEventAction, CalendarEventTimesChangedEvent } from 'angular-calendar';
import { User } from '../user';
import { UserService } from '../user.service';

@Component({
  selector: 'app-available-days',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './available-days.component.html',
  styleUrls: ['./available-days.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class AvailableDaysComponent implements OnInit {
  @ViewChild('modalContent') modalContent: TemplateRef<any>;

  selectedMonthViewDay: CalendarMonthViewDay;
  selectedDayViewDate: Date;
  selectedDays: any = [];
  
  view: string = 'month';

  viewDate: Date = new Date("09/01/2018");

  modalData: {
    action: string;
    event: CalendarEvent;
  };

  refresh: Subject<any> = new Subject();

  events: CalendarEvent[] = [];

  activeDayIsOpen: boolean = false;

  constructor(
    private userService: UserService,
    private modal: NgbModal) { }

  ngOnInit() {
  }

  dayClicked(day: CalendarMonthViewDay): void {
    this.selectedMonthViewDay = day;
    const selectedDateTime = this.selectedMonthViewDay.date.getTime();
    const dateIndex = this.selectedDays.findIndex(
      selectedDay => selectedDay.date.getTime() === selectedDateTime
    );
    if (dateIndex > -1) {
      delete this.selectedMonthViewDay.cssClass;
      this.selectedDays.splice(dateIndex, 1);
    } else {
      // Fixes a bug that causes an hour to be removed from the date during BST, resulting in the previous day being selected
      this.selectedMonthViewDay.date.setMinutes(this.selectedMonthViewDay.date.getMinutes() - this.selectedMonthViewDay.date.getTimezoneOffset());
      this.selectedDays.push(this.selectedMonthViewDay);
      day.cssClass = 'cal-day-selected';
      this.selectedMonthViewDay = day;
    }
  }

  eventTimesChanged({
    event,
    newStart,
    newEnd
  }: CalendarEventTimesChangedEvent): void {
    event.start = newStart;
    event.end = newEnd;
    this.handleEvent('Dropped or resized', event);
    this.refresh.next();
  }

  handleEvent(action: string, event: CalendarEvent): void {
    this.modalData = { event, action };
    this.modal.open(this.modalContent, { size: 'lg' });
  }

  addDates(name: string): void {
    name = name.trim();
    if(!name || this.selectedDays.length == 0)  { return; }
    var user: User = new User();
    user.name = name;
    user.days = this.selectedDays;
    this.userService.addAvailableDays(user).subscribe();
    this.refresh.next();
  }

}
