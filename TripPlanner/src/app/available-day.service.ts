import { Injectable } from '@angular/core';
import { AvailableDay } from './available-day';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { catchError, map, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class AvailableDayService {

  private availableDaysUrl = '/api/days';

  constructor(private http: HttpClient) { }

  getAvailableDays(): Observable<AvailableDay[]> {
    return this.http.get<AvailableDay[]>(this.availableDaysUrl)
      .pipe(
        catchError(this.handleError('getAvailableDays', []))
      );
  }

  addAvailableDay(day: AvailableDay): Observable<AvailableDay> {
    return this.http.post<AvailableDay>(this.availableDaysUrl, day, httpOptions)
      .pipe(
        catchError(this.handleError<AvailableDay>('addAvailableDay'))
      );
  }
  
  deleteAvailableDay(day: AvailableDay | number): Observable<AvailableDay> {
    const id = typeof day === 'number' ? day : day.id;
    const url = `${this.availableDaysUrl}/${id}`;

    return this.http.delete<AvailableDay>(url, httpOptions)
      .pipe(
        catchError(this.handleError<AvailableDay>('deleteAvailableDay'))
      );
  }

  updateAvailableDay(day: AvailableDay): Observable<any> {
    return this.http.put(this.availableDaysUrl, day, httpOptions)
      .pipe(
        catchError(this.handleError<any>('updateAvailableDay'))
      );
  }

  private handleError<T> (operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      console.error(error);

      return of(result as T);
    }
  }

}
