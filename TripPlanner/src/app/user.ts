import { AvailableDay } from "./available-day";

export class User {
    id: number;
    name: string;
    days: AvailableDay[];
}